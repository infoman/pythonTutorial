#Generamos una clase que herede de Exception
class UnoError(Exception):
    def __init__(self, valor):
        self.valorError = valor
    def __str__(self):
        print("No se puede dividir entre el número", valor)

print("Hola")
try:
    #Variable que no existe
    #print(l)
    #División entre cero
    #print(1/0)
    n = 4
    d = 1
    if n==1:
        raise UnoError(n)
except(TypeError, NameError):
    print("No existe la variable")
except ZeroDivisionError:
    print("División entre cero")
except UnoError: #Error creado por mi mismo
    print("Se ha producido un error que yo mismo cree")
else:
    print("No hubo error")
finally:
    print("Se ejecuta haya o no ocurrido un error")
print("Bienvenido")
