#Creamos una una excepcion definida por el desarrollador
class Err(Exception):
    def __init__(self, valor):
        print("El error fue por el ",valor)

try:
    raise Err(4)
except:
    print("Error escrito")
