class Intro:
    Introver = 8
    def __init__(self, valor):
        self.valor = valor
    def segundo(self):
        print("Segundo")
    def tercero(self):
        print("tercero")

class Intro2:
    pass

dato = Intro("Rafael")
print(dir(dato))

#Pregunta si la instancia dato pertenece a la clase Intro2
print(isinstance(dato, Intro2))

#Pregunta si la instancia dato, tiene el atributo Introver
print(hasattr(dato, "Introver"))
