lista = [2, 4]

#Capturar exepciones

try:
    print(lista[1])
except IndexError:
    print("Error de indice")
else:
    print("No hay errores")
finally:
    print("Se ejecuto correctamente")

#Lanzar exepciones

try:
    raise TypeError
except:
    print("Errores con los tipos")
