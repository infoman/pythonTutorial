import math as m
class Circulo:
    def __init__(self, radio):
        self.radio = radio

    #vuelve el método en  una propiedad
    @property
    def area(self):
        return(m.pi*self.radio**2)

c = Circulo(10)
#LLama al método somo si fuese una propiedad
print(c.area)
