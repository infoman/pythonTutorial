class Persona():
    #Variables de clase
    edad = 18
    def __init__(self, nombre, nacionalidad):
        #Variables de instancia
        self.nombre = nombre
        self.nacionalidad = nacionalidad

    #Método de instancia en python
    def nadar(self):
        print("Nadando")

#Para las variables de instancia hay que crear la instancia de la clase
persona1 = Persona("Rafael Ortiz", "boliviano")
print(persona1.nombre, persona1.nacionalidad, persona1.edad)
#Para las variables de instancia hay que crear la instancia de la clase
print(Persona.edad)
#Llamando al método de instancia
persona1.nadar()
