class Persona:
    suma = 0
    sumar1 = 0
    def __init__(self):
        pass

    @classmethod
    def correr(cls):
        print("Corra")

    #Para los métodos estaticos no se necesita ningún argumento
    @staticmethod
    def nadar():
        print("nado");

    @staticmethod
    def sumarStatic():
        Persona.suma = Persona.suma+1
        return(Persona.suma)

    def sumar(self):
        self.sumar1 = self.sumar1+1
        return(self.sumar1)

jose = Persona()
#jose.nadar()
jose1 = Persona()

#Con métodos estaticos
print("Con métodos estaticos")
print(jose.sumarStatic())
print(jose1.sumarStatic())
print(jose.sumarStatic())
print(jose1.sumarStatic())
print(jose.sumarStatic())
print(jose1.sumarStatic())
print(jose.sumarStatic())
#LLamada al método estatico sin crear una instancia de la clase
print(Persona.sumarStatic())

#Sin métodos estaticos
print("Sin métodos estaticos")
print(jose.sumar())
print(jose1.sumar())
print(jose.sumar())
print(jose1.sumar())
print(jose.sumar())
print(jose1.sumar())

"""
Resultados
----------
Con métodos estaticos
1
2
3
4
5
6
7
Sin métodos estaticos
1
1
2
2
3
3

"""
