class Persona:
    edad = 18
    def __init__(self):
        pass
    @classmethod
    #Método de clase
    def saludar(cls, nombre):
        print("Estoy saludando a "+nombre)

    def hola(self):
        print("Método de instancia")

#Llamando al método clase
Persona.saludar("Rafael")
p = Persona()
p.hola()
