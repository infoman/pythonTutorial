#Condicional if sin parentesis
edad = 20
if edad > 15:
    print ("Si es mayor de edad")
else:
    print ('No es mayor de edad')
print("Este mensaje siempre se mostrar")

#Condicional if con partentesis
switch = False
if(switch):
    print("Es verdad")
else:
    print("Es falso")

#Condicional con AND
if not switch and edad>18:
    print("Es mayor de edad y es verdadero")

#elseif
var = 10
if var > 32:
    print("Mayor a 10")
elif var > 20:
    print("Mayor que 20")
elif var > 30:
    print("Mayor que 30")
elif var > 40:
    print("Mayor que 40")
else:
    print("Menor que 10")
