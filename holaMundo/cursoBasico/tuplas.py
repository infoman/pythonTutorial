#Tuplas en Python
#La tupla es inmutable, no se pueden cambiar los elementos en las tuplas dentro de Python

tupla = (27, 1.68, "Rafael Ortiz")

print(tupla[2])

#Recorrido de la tupla
i = 0
while(i<len(tupla)):
    print(tupla[i])
    i = i+1

#Recorrido con for
tupla2 = (10, 20, 30, 40, 50, 60)

for numeros in tupla2:
    print(numeros)

#Porciones en las tuplas
tupla3 = (1,2,3,4,5,6,7,8,9,10,11,12,13)
#Cuenta de la posicion 0 hasta la poscion 3, sin contar la misma (3)
#tupla4 = tupla3[0:3] #También se puede escribir de la siguiente forma tupla4 = tupla3[:3]
tupla4 = tupla3[:3]
print(tupla4)#Muestra 1,2,3
#Cuenta de la posicion 2 hasta la poscion 5, sin contar la misma (5)
tupla5 = tupla3[2:5]#Muestra 3,4,5
print(tupla5)
#Cuenta posiciones desde la posicion 6 hasta el final
tupla6 = tupla3[6:]
print(tupla6)
