#While con parentesis
contador = 1
while(contador <= 10):
    print(contador)
    contador = contador+1

#While sin parentesis
contador = 1
booleano = True
while booleano:
    if contador==10:
        booleano = False
    print(contador)
    contador = contador+1
