lista = ['uno', 'dos', 'tres', 'cuatro']
#Tamaño de la lista
print(len(lista))

#Recorrido de la lista
for elementos in lista:
    print(elementos)

#Creamos una segunda lista
lista2 = [1, 2, 3, '4']

#Concatenamos lista y lista2 en lista3
lista3 = lista2+lista
print(lista3)

#Preguntamos si el elemento 4 existe en lista3
if 4 in lista3:
    print("Si existe")
else:
    print("No existe")

#Porciones en listas
lista4 = ['Rafael', 'Juan', 'Pedro', 'Pablo', 'Mateo']
print(lista4[1:3])
print(lista4[0])

#Indices Negativos
frutas = ['platano', 'manzana', 'uva', 'higo']
print(frutas)
#Con -1 estamos sancando el último elemento
print(frutas[-1])
#Con -2 estamos sancando el penúltimo elemento
print(frutas[-2])
