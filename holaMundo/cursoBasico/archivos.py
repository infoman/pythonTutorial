#Creación de archivos
def crearArchivo():
    #Si no lo abre, crea el archivo, el parámetro w indica con permisos de escritura
    archivo = open('datos.txt', 'w')
    archivo.close()

#Escribimos el archivo
def escribirArchivo():
    #Abrimos el archivo en modo de (a)ppend, asi de esta forma lo que tiene el archivo no se toca, solo se añade
    archivo = open('nuevoDatos.txt', 'a')
    #Escribimos un salto de linea
    archivo.write("\n")
    archivo.write("Rafael Ortiz Rocha")
    archivo.write("\n")
    archivo.write("76582979")
    archivo.close()

def leerArchivo():
    #abrimos el archivo nuevoDatos.txt en modo de lectura
    archivo=open('nuevoDatos.txt', 'r')
    linea = archivo.readline()
    #Hace el recorrido linea por linea hasta que linea se igual a vacio
    #OJO vacio no es lo mismo que espacio o salto de linea
    while linea != "":
        print(linea)
        linea = archivo.readline()

leerArchivo()
#escribirArchivo()
#Llamamos a la función
#crearArchivo()
