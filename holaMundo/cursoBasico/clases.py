#Clase vacia
#class Persona:
#    pass

class Persona:
    nBrazos = 0
    nPiernas = 0
    cabello = True
    cCabello = 'Negro'
    hambre = 0

    #Creamos el constructor
    def __init__(self):
        self.nBrazos = 2
        self.nPiernas = 2

    #Metodo dormir no hace nada
    def dormir():
        pass

    #self al igual que this, hace referencia al mismo objeto
    def comer(self, h):
        self.hambre = h


#La clase Hombre hereda atributos y métodos de la clase Persona
class Hombre(Persona):
    nombre = 'Rafael Ortiz'
    sexo = 'Masculino'

    def cambiarNombre(self, nom):
        self.nombre = nom

class Mujer(Persona):
    nombre = 'Juana de Arco'
    sexo = 'Femenino'

##Instanciamos el objeto Hombre
jose = Hombre()

jose.cambiarNombre('Pedro');
print(jose.hambre)
jose.comer(5)
print(jose.hambre)
print(jose.nombre)
