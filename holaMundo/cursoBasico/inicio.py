#enteros
edad = 27

#Reales
altura = 1.77

#Cadenas
cad = "Hola mundo"
cad2 = 'Hola mundo 2'

#Booleanos
flag = True
flag2 = False

#Números complejos
numeroc =   7+8j

print(numeroc)

#Operadores matemáticos y comentarios
x = 10
y = 2
#suma
print(x+y)

#resta
print (x-y)

#multiplicación
print(x*y)

#División
print(x/y)

#División con redondeo al inmediato inferior
print(x//y)

#Modulo
print(x%y)

#Exponencial
print(x**y)
