def saludo():
    print("Hola mundo")
#Llamamos a la función
saludo()

def saludo(nombre):
    print("hola",nombre)
saludo("Rafael")

def suma(a, b):
    res = a+b
    print(res)
suma(4, 5)

def division(a, b):
    return(a/b)
print(division(10,2)+9)

#Los valores por defecto simpre se colocan de derecha a izquierda
def multiplicacion(a, b = 1):
    return a*b
multiplicacion(3)
