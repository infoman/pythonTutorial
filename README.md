# PYTHON

## Programas Basicos

Python tiene tres formas de trabajo

- Shell
- Por lienas de comando CMD

## Varibles en Python

Los tipos de variables que existen en Python son:

- Enteros
- Reales y/o flotantes
- Boolean
- Cadenas

~~~python
#enteros
edad = 27

#Reales
altura = 1.77

#Cadenas
cad = "Hola mundo"
cad2 = 'Hola mundo 2'

#Booleanos
flag = True
flag2 = False

#Números complejos
numeroc =   7+8j

print(numeroc)
~~~

## Operadores matemáticos y comentarios

~~~python
#Operadores matemáticos y comentarios
x = 10
y = 2
#suma
print(x+y)

#resta
print (x-y)

#multiplicación
print(x*y)

#División
print(x/y)

#División con redondeo al inmediato inferior
print(x//y)

#Modulo
print(x%y)

#Exponencial
print(x**y)
~~~

## Condicional IF

En python se puede o no utilizar paréntesis. La indentación es importante en esta herramienta.

~~~python
#Condicional if sin parentesis
edad = 20
if edad > 15:
    print ("Si es mayor de edad")
else:
    print ('No es mayor de edad')
print("Este mensaje siempre se mostrar")

#Condicional if con partentesis
switch = False
if(switch):
    print("Es verdad")
else:
    print("Es falso")

# Condicional con AND
if not switch and edad>18:
    print("Es mayor de edad y es verdadero")

#elseif
var = 10
if var > 32:
    print("Mayor a 10")
elif var > 20:
    print("Mayor que 20")
elif var > 30:
    print("Mayor que 30")
elif var > 40:
    print("Mayor que 40")
else:
    print("Menor que 10")
~~~

## Operadores relacionales

- (>) Mayor
- (>=) Mayor o igual
- (==) Igual
- (!=) Distinto

## Operadores lógicos

- and
- or
- not

## Sentencia WHILE

~~~python
#While con parentesis
contador = 1
while(contador <= 10):
    print(contador)
    contador = contador+1

#While sin parentesis
contador = 1
booleano = True
while booleano:
    if contador==10:
        booleano = False
    print(contador)
    contador = contador+1
~~~

## Funciones en Python

~~~python
def saludo():
    print("Hola mundo")
#Llamamos a la función
saludo()
~~~

## Funciones que reciben variables

~~~python
def saludo(nombre):
    print("hola",nombre)
saludo("Rafael")

def suma(a, b):
    res = a+b
    print(res)
suma(4, 5)
~~~

## Funciones que regresan valores

~~~python
def division(a, b):
    return(a/b)
print(division(10,2))
~~~

## Funciones con parámetros por defecto

~~~python
#Los valores por defecto simpre se colocan de derecha a izquierda
def multiplicacion(a, b = 1):
    return a*b
multiplicacion(3)
~~~

## Tuplas

~~~python
#Tuplas en Python
#La tupla es inmutable, no se pueden cambiar los elementos en las tuplas dentro de Python

tupla = (27, 1.68, "Rafael Ortiz")

print(tupla[2])

#Recorrido de la tupla
i = 0
while(i<len(tupla)):
    print(tupla[i])
    i = i+1
~~~

## Tuplas con FOR

~~~python
#Recorrido con for
tupla2 = (10, 20, 30, 40, 50, 60)

for numeros in tupla2:
    print(numeros)
~~~

## Porciones en las tuplas

~~~python
#Porciones en las tuplas
tupla3 = (1,2,3,4,5,6,7,8,9,10,11,12,13)
#Cuenta de la posicion 0 hasta la poscion 3, sin contar la misma (3)
#tupla4 = tupla3[0:3] #También se puede escribir de la siguiente forma tupla4 = tupla3[:3]
tupla4 = tupla3[:3]
print(tupla4)#Muestra 1,2,3
#Cuenta de la posicion 2 hasta la poscion 5, sin contar la misma (5)
tupla5 = tupla3[2:5]#Muestra 3,4,5
print(tupla5)
#Cuenta posiciones desde la posicion 6 hasta el final
tupla6 = tupla3[6:]
print(tupla6)
~~~

## Manejo de cadenas en Python

~~~python
cad = "Rafael Freddy Ortiz Rocha"

#Imprimimos Rafael
print(cad[:6])
#Imprimimos Freddy
print(cad[7:13])
#Imprimimos Ortiz
print(cad[14:19])
print(cad[20:])

i = 0
while i<=5:
    print(cad[i])
    i = i+1

#Resulados
"""
Rafael
Freddy
Ortiz
Rocha
R
a
f
a
e
l
"""
~~~

## Listas

Las listas a diferencia de las tuplas, se pueden cambiar de valores

~~~python
lista = ['uno', 'dos', 'tres', 'cuatro']
#Tamaño de la lista
print(len(lista))

#Recorrido de la lista
for elementos in lista:
    print(elementos)

#Creamos una segunda lista
lista2 = [1, 2, 3, '4']

#Concatenamos lista y lista2 en lista3
lista3 = lista2+lista
print(lista3)

#Preguntamos si el elemento 4 existe en lista3
if 4 in lista3:
    print("Si existe")
else:
    print("No existe")

#Porciones en listas
lista4 = ['Rafael', 'Juan', 'Pedro', 'Pablo', 'Mateo']
print(lista4[1:3])
print(lista4[0])
~~~

## Indices negativos en tuplas

Los indices negativos, nos permiten acceder a los elementos de una lista, tupla, cadena o cualquiera que tenga un indice.

~~~python
#Indices Negativos
frutas = ['platano', 'manzana', 'uva', 'higo']
print(frutas)
#Con -1 estamos sancando el último elemento
print(frutas[-1])
#Con -2 estamos sancando el penúltimo elemento
print(frutas[-2])
~~~

## Diccionarios

En los diccionarios nosotros colocamos el indice.

~~~python
diccionario = {
    'Rafael': 30,
    'Juan': 28,
    'Pedro': 31
}
print("El tamaño del diccionario es",len(diccionario))
print(diccionario['Rafael'])
print(diccionario['Juan'])
print(diccionario['Pedro'])
~~~

## Función HELP

La función help, nos indica que métodos tiene dicho objeto.

~~~python
nombres = ['juan', 'perez', 'Gonzalo']
#La función help indica que metodos tiene dicho objeto
help(nombres)
~~~

## Función TYPE

Devuelve el tipo de variable a la que corresponde dicho valor.

~~~python
x = True
print(type(x))
decimal = 3.141516
print(type(decimal))
nombre = "Rafael Freddy Ortiz Rocha"
print(type(nombre))
complejo = 3+4j
print(type(complejo))
diccionario = {
    'platano': 50,
    'naranja': 30,
    'manzana': 10
}
print(type(diccionario))
~~~

## Función STR

La función ***str*** convierte cualquier valor a cadena.

~~~python
a = 5
b = 4
c = a+b
print("La suma es "+str(c))
~~~

## Comando DIR

Nos ayuda a saber que funciones tenemos para dichos tipos de variables.

~~~python
tupla = (1,2,3,4,5)
#print(dir(tupla))

cad = "Hola mundo"
print(dir(cad))
~~~

## Programación Orientada a Objetos

### Clases en Python

En python las clases no pueden estar vacías, al menos que se coloque la instrucción ***pass***, como se ve en el ejemplo:

~~~python
#Clase vacia
class Persona:
    pass
~~~

### Atributos en Python

~~~python
class Persona:
    nBrazos = 0
    nPiernas = 0
    cabello = True
    cCabello = 'Negro'

class Hombre:
    nombre = 'Rafael Ortiz'
    sexo = 'Masculino'
~~~

### Métodos en Python

~~~python
class Persona:
    nBrazos = 0
    nPiernas = 0
    cabello = True
    cCabello = 'Negro'
    hambre = 0

    #Metodo dormir no hace nada
    def dormir():
        pass

    #self al igual que this, hace referencia al mismo objeto
    def comer(self):
        self.hambre = 1



class Hombre:
    nombre = 'Rafael Ortiz'
    sexo = 'Masculino'

    def cambiarNombre(self, nom):
        self.nombre = nom
~~~

### Constructores en Python

Nos permite inicial el objeto.

~~~python
class Persona:
    nBrazos = 0
    nPiernas = 0
    cabello = True
    cCabello = 'Negro'
    hambre = 0

    #Creamos el constructor
    def __init__(self):
        self.nBrazos = 2
        self.nPiernas = 2
~~~

### Herencia en Python

Existen dos tipos de herencias:

- Una es la **herencia simple** donde la clase padre hereda todos sus atributos y métodos a la clase hijo.

- La otra es la **herencia múltiple** donde varias clases padre pueden heredar todos sus atributos y métodos a una clase hijo.

~~~python
class Persona:
    nBrazos = 0
    nPiernas = 0
    cabello = True
    cCabello = 'Negro'
    hambre = 0

    #Creamos el constructor
    def __init__(self):
        self.nBrazos = 2
        self.nPiernas = 2

    #Metodo dormir no hace nada
    def dormir():
        pass

    #self al igual que this, hace referencia al mismo objeto
    def comer(self, h):
        self.hambre = h


#La clase Hombre hereda atributos y métodos de la clase Persona
class Hombre(Persona):
    nombre = 'Rafael Ortiz'
    sexo = 'Masculino'

    def cambiarNombre(self, nom):
        self.nombre = nom

class Mujer(Persona):
    nombre = 'Juana de Arco'
    sexo = 'Femenino'

##Instanciamos el objeto Hombre
jose = Hombre()

#Llamamos a los métodos de la clase Hombre
jose.cambiarNombre('Pedro');
print(jose.hambre)
jose.comer(5)
print(jose.hambre)
print(jose.nombre)
~~~

## Archivos en Python

### Crear Archivos

~~~python
#Creación de archivos
def crearArchivo():
    #Si no lo abre, crea el archivo, el parámetro w indica con permisos de escritura
    archivo = open('datos.txt', 'w')
    archivo.close()

#Llamamos a la función
crearArchivo()
~~~

### Escribir Archivos

~~~python
#Escribimos el archivo
def escribirArchivo():
    #Abrimos el archivo en modo de (a)ppend, asi de esta forma lo que tiene el archivo no se toca, solo se añade
    archivo = open('nuevoDatos.txt', 'a')
    #Escribimos un salto de linea
    archivo.write("\n")
    archivo.write("Rafael Ortiz Rocha")
    archivo.write("\n")
    archivo.write("76582979")
    archivo.close()

escribirArchivo()
~~~

### Leer un archivo

~~~python
def leerArchivo():
    #abrimos el archivo nuevoDatos.txt en modo de lectura
    archivo=open('nuevoDatos.txt', 'r')
    linea = archivo.readline()
    #Hace el recorrido linea por linea hasta que linea se igual a vacio
    #OJO vacio no es lo mismo que espacio o salto de linea
    while linea != "":
        print(linea)
        linea = archivo.readline()

leerArchivo()
~~~

### Variables de clase y variables de instancia

Para llamar a las variables de instancia, necesariamente se debe crear una instancia de la clase

~~~python
lass Persona():
    #Variables de clase
    edad = 18
    def __init__(self, nombre, nacionalidad):
        #Variables de instancia
        self.nombre = nombre
        self.nacionalidad = nacionalidad

#Para las variables de instancia hay que crear la instancia de la clase
persona1 = Persona("Rafael Ortiz", "boliviano")
print(persona1.nombre, persona1.nacionalidad, persona1.edad)
#Para las variables de instancia hay que crear la instancia de la clase
print(Persona.edad)
~~~

### Métodos de instancia en Python

Para declarar un método en python se sigue la siguiente sintaxis:

~~~python
class Persona():
    #Variables de clase
    edad = 18
    def __init__(self, nombre, nacionalidad):
        #Variables de instancia
        self.nombre = nombre
        self.nacionalidad = nacionalidad

    #Método de instancia en python
    def nadar(self):
        print("Nadando")

#Para las variables de instancia hay que crear la instancia de la clase
persona1 = Persona("Rafael Ortiz", "boliviano")
print(persona1.nombre, persona1.nacionalidad, persona1.edad)
#Para las variables de instancia hay que crear la instancia de la clase
print(Persona.edad)
#Llamando al método de instancia
persona1.nadar()
~~~

### Métodos de clase en Python

Cuando se utiliza un método de clase, es necesario agregar la referencias ***cls*** como argumento de dicho método.

~~~python
class Persona:
    edad = 18
    def __init__():
        pass

    @classmethod
    #Método de clase
    def saludar(cls, nombre):
        print("Estoy saludando a "+nombre)

#Llamando al método clase
Persona.saludar("Rafael")
~~~

### Métodos estáticos

Los métodos estáticos pueden ser llamados sin crear una instancia de la clase 

~~~python
class Persona:
    suma = 0
    sumar1 = 0
    def __init__(self):
        pass

    @classmethod
    def correr(cls):
        print("Corra")

    #Para los métodos estaticos no se necesita ningún argumento
    @staticmethod
    def nadar():
        print("nado");

    @staticmethod
    def sumarStatic():
        Persona.suma = Persona.suma+1
        return(Persona.suma)

    def sumar(self):
        self.sumar1 = self.sumar1+1
        return(self.sumar1)

jose = Persona()
#jose.nadar()
jose1 = Persona()

#Con métodos estaticos
print("Con métodos estaticos")
print(jose.sumarStatic())
print(jose1.sumarStatic())
print(jose.sumarStatic())
print(jose1.sumarStatic())
print(jose.sumarStatic())
print(jose1.sumarStatic())
print(jose.sumarStatic())
#LLamada al método estatico sin crear una instancia de la clase
print(Persona.sumarStatic())

#Sin métodos estaticos
print("Sin métodos estaticos")
print(jose.sumar())
print(jose1.sumar())
print(jose.sumar())
print(jose1.sumar())
print(jose.sumar())
print(jose1.sumar())

"""
Resultados
----------
Con métodos estaticos
1
2
3
4
5
6
7
Sin métodos estaticos
1
1
2
2
3
3

"""
~~~ 

### Métodos especiales

~~~python
class Clase:

    def __new__(cls):
        print("new")
        return(super().__new__(cls))
    #Inicializar variables de la clase
    def __init__(self):
        print("init")

c = Clase()
~~~

### Propiedades en python

~~~python
import math as m
class Circulo:
    def __init__(self, radio):
        self.radio = radio

    #vuelve el método en  una propiedad
    @property
    def area(self):
        return(m.pi*self.radio**2)

c = Circulo(10)
#LLama al método como si fuese una propiedad
print(c.area)
~~~

### Introspección en Python

Con la función ***dir*** dentro de Python se puede saber mas acerca de la clase con la que se esta trabajando

~~~python
class Intro:
    Introver = 8
    def __init__(self, valor):
        self.valor = valor
    def segundo(self):
        print("Segundo")
    def tercero(self):
        print("tercero")

class Intro2:
    pass

dato = Intro("Rafael")
print(dir(dato))

#Pregunta si la instancia dato pertenece a la clase Intro2
print(isinstance(dato, Intro2))

#Pregunta si la instancia dato, tiene el atributo Introver
print(hasattr(dato, "Introver"))
~~~

## Manejo de Excepciones en Python

Las excepciones en Python, no permiten que se finalice por mas que haya un error.

Por ejemplo si se desea concatenar una cadena con un número, el error lanzado será de tipo ***typeError***, o si se desearía utilizar una variable que no existe el error será de tipo ***nameError***.

Para encontrar mas información acercad de las excepciones de python 3.7 se puede ir al siguiente [aquí](https://docs.python.org/3.7/library/exceptions.html).

### Capturar Excepciones

~~~python
try:
    print(lista[1])
except IndexError:
    print("Error de indice")
else:
    print("No hay errores")
finally:
    print("Se ejecuto correctamente")
~~~

### Lanzar Excepciones

~~~python
try:
    raise TypeError
except:
    print("Errores con los tipos")
~~~

### Crear Excepciones

~~~python
class Err(Exception):
    def __init__(self, valor):
        print("El error fue por el ",valor)

try:
    raise Err(4)
except:
    print("Error escrito")
~~~

A continuación se muestra un ejemplo de todo lo repasado anteriormente.

~~~python
#Generamos una clase que herede de Exception
class UnoError(Exception):
    def __init__(self, valor):
        self.valorError = valor
    def __str__(self):
        print("No se puede dividir entre el número", valor)


print("Hola")
try:
    #Variable que no existe
    #print(l)
    #División entre cero
    #print(1/0)
    n = 4
    d = 1
    if n==1:
        raise UnoError(n)
except(TypeError, NameError):
    print("No existe la variable")
except ZeroDivisionError:
    print("División entre cero")
except UnoError: #Error creado por mi mismo
    print("Se ha producido un error que yo mismo cree")
else:
    print("No hubo error")
finally:
    print("Se ejecuta haya o no ocurrido un error")
print("Bienvenido")
~~~

### Paquetes

El ***import*** no solo hace que tengamos disponible todo lo definido dentro del módulo, sino que también ejecuta el código del módulo.

La clausula ***import*** también permite importar varios módulos en la
misma línea.

~~~python
import os, sys, time
~~~














